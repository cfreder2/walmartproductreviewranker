package test.java.WPS;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import main.java.WPS.ProductReviewRanker;
import main.java.WPS.WalmartService;
import main.java.WPS.beans.Item;
import main.java.WPS.beans.Review;
import main.java.WPS.beans.SearchResponse;

public class TestWalmart {
    WalmartService service;
    ProductReviewRanker ranker;
    SearchResponse response;
    Item ipod;
    Review rev;
    ArrayList<Review> revs;

    @Before
    public void setUp() {
	service = new WalmartService();
	ranker = new ProductReviewRanker();
	ipod = new Item();
	ipod.setItemId(42608121L);
	response = null;
	rev = null;
    }

    @After
    public void tearDown() {
	service = null;
    }

    @Test
    public void testSearchByString() {
	response = service.searchByString("ipod");
	assertNotNull("searchByString() not returning an object.", response);
	assertNotNull("searchByString() response object holds no items.",
		response.getItems());
	assertNotNull("searchByString() response object fields not populated.",
		response.getItems().get(0).getItemId());
    }

    @Test
    public void testGetProductRecommendations() {
	ArrayList<Item> recs = service.getProductRecommendations(ipod);
	assertNotNull("getProductRecommendations() is returing an object.",
		recs);
	assertNotNull("getProductRecommendations() fields not populated.", recs
		.get(0).getItemId());

    }

    @Test
    public void testGetItemReview() {
	rev = service.getItemReview(ipod);
	assertNotNull("getReviews() is returning an object.", rev);
	assertNotNull(
		"getReviews() response object holds no reviewStatistics object.",
		rev.getReviewStatistics());
	assertNotNull("getReviews() reviewStatistics fields not populated.",
		rev.getReviewStatistics().getAverageOverallRating());
    }

    @Test
    public void testProcessSearch() {
	Item i = service.processSearch("ipod");
	assertNotNull("processSearch() is returning an object.", i);
	assertNotNull("processSearch() object fields not populated.",
		i.getItemId());
    }

    @Test
    public void testProcessAndRankReviews() {
	revs = service.processAndRankReviews(service
		.getProductRecommendations(ipod));
	for (int i = 0; i < revs.size() - 1; i++) {
	    assertTrue(
		    "Review are not ordered from highest to lowest as expected.",
		    revs.get(i).getReviewStatistics().getAverageOverallRating() >= revs
			    .get(i + 1).getReviewStatistics()
			    .getAverageOverallRating());
	}
    }

    @Test
    public void testReviewTostring() {
	//this test was initially created because there was an exception being thrown by the toString() and 
	// it was necessary to test it in this fashion
	rev = service.processAndRankReviews(
		service.getProductRecommendations(ipod), 1).get(0);
	try {
	    rev.toString();
	} catch (Exception e) {
	    fail("Exception caught in creation of review string output.");
	}
    }
}
