package main.java.WPS.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Item {
    private String name;
    private long itemId;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public long getItemId() {
	return itemId;
    }

    public void setItemId(long itemId) {
	this.itemId = itemId;
    }
    
    @Override
    public String toString() {
	return String.valueOf(itemId);
    }
}
