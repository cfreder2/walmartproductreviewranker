package main.java.WPS.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReviewStatistics implements Comparable<ReviewStatistics>{

    private double averageOverallRating;

    public double getAverageOverallRating() {
	return averageOverallRating;
    }

    public void setAverageOverallRating(double averageOverallRating) {
	this.averageOverallRating = averageOverallRating;
    }

    public int compareTo(ReviewStatistics rev) {
	double revRating = rev.getAverageOverallRating(); 
	if (this.averageOverallRating > revRating) {
	    return 1;
	}
	else if (this.averageOverallRating < revRating) {
	    return -1;
	}
	else return 0;
    }
    
    @Override
    public String toString() {
	return String.valueOf(averageOverallRating);
    }
}
