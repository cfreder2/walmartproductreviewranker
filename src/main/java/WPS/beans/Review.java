package main.java.WPS.beans;

import java.text.NumberFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Review implements Comparable<Review> {

    private long itemId;
    private String name;
    private double salePrice;
    private String productUrl;
    private ReviewStatistics reviewStatistics;

    public long getItemId() {
	return itemId;
    }

    public void setItemId(long itemId) {
	this.itemId = itemId;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public double getSalePrice() {
	return salePrice;
    }

    public void setSalePrice(double salePrice) {
	this.salePrice = salePrice;
    }

    public String getProductUrl() {
	return productUrl;
    }

    public void setProductUrl(String productUrl) {
	this.productUrl = productUrl;
    }

    public ReviewStatistics getReviewStatistics() {
	return reviewStatistics;
    }

    public void setReviewStatistics(ReviewStatistics reviewStatistics) {
	this.reviewStatistics = reviewStatistics;
    }

    public int compareTo(Review rev) {
	return this.reviewStatistics.compareTo(rev.reviewStatistics);
    }

    @Override
    public String toString() {
	NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();

	StringBuilder str = new StringBuilder();
	str.append("\n");
	str.append(name);
	str.append("\nItem ID: ");
	str.append(itemId);
	str.append("\nSale price: ");
	str.append(currencyFormatter.format(salePrice));
	str.append("\nAverage Overall Rating: ");
	str.append(reviewStatistics.toString());
	str.append("\nProduct URL: ");
	str.append(productUrl);
	return str.toString();
    }

}
