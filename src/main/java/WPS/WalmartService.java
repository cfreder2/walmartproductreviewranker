package main.java.WPS;

import java.util.ArrayList;
import java.util.Collections;

import main.java.WPS.beans.Item;
import main.java.WPS.beans.Review;
import main.java.WPS.beans.SearchResponse;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

public class WalmartService {

    private RestTemplate restTemplate;
    
    public WalmartService(){
	restTemplate = new RestTemplate();
    }
    
    public SearchResponse searchByString(String searchTerm){
	return restTemplate.getForObject(WalmartAPITools.getSearchURL(searchTerm), SearchResponse.class);
    }
    
    public ArrayList<Item> getProductRecommendations(Item item) {
	return restTemplate.exchange(
		WalmartAPITools.getProductRecommendationURL(item), HttpMethod.GET, null,
		new ParameterizedTypeReference<ArrayList<Item>>() {}).getBody();
    }
    
    public Review getItemReview(Item item) {	
	return restTemplate.getForObject(WalmartAPITools.getReviewsURL(item), Review.class);
    }
    
    public Item processSearch(String searchTerm) {	
	return searchByString(searchTerm).getItems().get(WalmartAPITools.ITEM_TO_USE);
    }
    
    public ArrayList<Review> processAndRankReviews(ArrayList<Item> items) {
	ArrayList<Review> overallReviews = new ArrayList<Review>(WalmartAPITools.PRODUCTS_TO_REVIEW);
	
	for (int i = 0; i < WalmartAPITools.PRODUCTS_TO_REVIEW; i++) {
	    overallReviews.add(getItemReview(items.get(i)));
	}
	Collections.sort(overallReviews);
	Collections.reverse(overallReviews);
	return overallReviews;
    }
    
    public ArrayList<Review> processAndRankReviews(ArrayList<Item> items, int productsToReview) {
	ArrayList<Review> overallReviews = new ArrayList<Review>(productsToReview);
	
	for (int i = 0; i < productsToReview; i++) {
	    overallReviews.add(getItemReview(items.get(i)));
	}
	Collections.sort(overallReviews);
	Collections.reverse(overallReviews);
	return overallReviews;
    }
    
    public void outputToConsole(Item searchItem, ArrayList<Review> output) {
	System.out.print("Recommended items ranked by average overall review rating for ");
	System.out.println(searchItem.getName());
	for (Review r: output) {
	    System.out.println(r.toString());
	}
    }
}
