package main.java.WPS;

import main.java.WPS.beans.Item;

public class WalmartAPITools {

    public static final String API_KEY = "gs7x5mpdrpdwctvpwv3hpjka";
    public static final String API_KEY2 = "fpz56qpvq2s3ev8h5rh62pk3";
    
    public static final int ITEM_TO_USE = 1;
    public static final int PRODUCTS_TO_REVIEW = 10;
    
    public static final String URL_PREFIX = "http://api.walmartlabs.com/v1/";
    
    public static String getSearchURL(String searchTerm) {
	StringBuilder url = new StringBuilder(URL_PREFIX);
	url.append("search?apiKey=");
	url.append(API_KEY);
	url.append("&query=");
	url.append(searchTerm);
	return url.toString();
    }
    
    public static String getProductRecommendationURL(Item item) {
	StringBuilder url = new StringBuilder(URL_PREFIX);
	url.append("nbp?apiKey=");
	url.append(API_KEY);
	url.append("&itemId=");
	url.append(item.getItemId());
	return url.toString();
    }
    
    public static String getReviewsURL(Item item) {
	StringBuilder url = new StringBuilder(URL_PREFIX);
	url.append("reviews/");
	url.append(item.getItemId());
	url.append("?apiKey=");
	url.append(API_KEY);
	return url.toString();
    }
}
