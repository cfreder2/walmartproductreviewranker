package main.java.WPS;

import java.util.ArrayList;
import java.util.Scanner;

import main.java.WPS.beans.Item;

import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductReviewRanker implements CommandLineRunner{

    public static void main(String[] args) {
	SpringApplication app = new SpringApplication(ProductReviewRanker.class);
	app.setBannerMode(Banner.Mode.OFF); //Disable Spring default console output banner
	app.run();
    }
    
    public void run(String... strings) throws Exception {
	WalmartService service = new WalmartService();
	Scanner scanner = new Scanner(System.in);
	System.out.println("What item would you like to query for?");
	String itemString = scanner.next();
	System.out.println("Querying...");
	Item response = service.processSearch(itemString);
	System.out.println("Looking for product recommendations...");
	ArrayList<Item> recommendations = service.getProductRecommendations(response);
	service.outputToConsole(response, service.processAndRankReviews(recommendations));
	scanner.close();
    }
    
}
