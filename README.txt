Walmart Product Review Ranker
by Casey Frederick

This project uses Maven for compilation and dependency management, Spring web for consuming the Walmart REST web service open API, 
Spring Boot for application management and Jackson for parsing JSON. It could be refactored quickly to provide 
more information by adding fields to the beans. Currently, it interfaces with the console to consume command line input and writes 
output back to console. It could be refactored easily to write to a file or any other medium by omitting the output function call 
in the WalmartService class. Junit tests are included in the TestWalmart class and can be run by importing the project into Eclipse 
and running them individually using Eclipse' run/debug as Junit test functionality, or by installing Maven and running the command below. 
In the context of this program, the application.properties file was used to disable debug output to console, because it clutters the actual 
program output.

Assumptions:
Average overall rating would be the best metric to rank reviews by in this context.

To run application:
From root directory, java -jar target/WalmartProductReviewRanker-0.1.0.jar

To build solution (requires Maven installation & PATH reference):
From root directory, mvn clean package

To run test suite (requires Maven installation & PATH reference):
From root directory, mvn test -Dtest=TestWalmart